import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
} from 'react-native';
import React from 'react';
import {Banner, ImageHeader, Profil} from '../../assets';
import ButtonIcon from '../../components/ButtonIcon';
import {ScrollView} from 'react-native-gesture-handler';
import ListMobil from '../../components/ListMobil';

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <View style={styles.hello}>
            <Text style={styles.name}>Timothy Kaleb Kurniawan</Text>
            <Text style={styles.location}>Semarang, Jawa Tengah</Text>
          </View>
          <Image source={Banner} style={styles.image} />
        </ImageBackground>
        <View style={styles.menu}>
          <View style={styles.buttonMenu}>
            <ButtonIcon title="Sewa Mobil" />
            <ButtonIcon title="Oleh-Oleh" />
            <ButtonIcon title="Penginapan" />
            <ButtonIcon title="Wisata" />
          </View>
        </View>
        <View style={styles.daftar}>
          <Text style={styles.location}>Daftar Mobil Pilihan</Text>
          <ListMobil />
          <ListMobil />
          <ListMobil />
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  profil: {
    justifyContent: 'center',
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.25,
  },
  hello: {
    marginTop: windowHeight * 0.02,
    marginLeft: windowHeight * 0.02,
  },
  name: {
    fontSize: 16,
    fontFamily: 'Helvetica',
    color: '#000000',
  },
  location: {
    fontSize: 20,
    fontFamily: 'Helvetica-Bold',
    color: '#000000',
  },
  image: {
    margin: windowHeight * 0.02,
    width: windowWidth * 0.9,
    height: windowHeight * 0.22,
    borderRadius: 15,
  },
  menu: {
    backgroundColor: 'white',
    marginTop: windowHeight * 0.11,
    marginHorizontal: windowHeight * 0.02,
    width: windowWidth * 0.9,
    height: windowHeight * 0.1,
    borderRadius: 15,
    // paddingHorizontal:30,
    // flexDirection: 'row',
  },
  buttonMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  daftar: {
    marginTop: windowHeight * 0.08,
    marginLeft: windowHeight * 0.02,
    flex: 1,
  },
});
