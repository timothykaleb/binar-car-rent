import {StyleSheet, Text, View, Dimensions} from 'react-native';
import React from 'react';
import ListMobil from '../../components/ListMobil';
import {ScrollView} from 'react-native-gesture-handler';

const List = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.text}>
          <Text style={styles.judul}>Daftar Mobil</Text>
        </View>
        <View style={styles.list}>
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
        </View>
      </ScrollView>
    </View>
  );
};

export default List;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  judul: {
    fontSize: 25,
    fontFamily: 'Helvetica-Bold',
    color: '#000000',
    marginBottom: 10,
  },
  list: {
    marginHorizontal: windowWidth * 0.02,
    alignItems: 'center',
  },
  text: {
    marginHorizontal: windowWidth * 0.02,
  },
});
