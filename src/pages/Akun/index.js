import {StyleSheet, Text, View, Image, Dimensions, Button} from 'react-native';
import React from 'react';
import {Park} from '../../assets';

const Akun = () => {
  return (
    // <View style={styles.page}>
    // <Image style={styles.image} source={Park} />
    // <Text style={styles.text}>
    //   Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
    //   lebih mudah
    // </Text>
    // <Button title="Register" color="green" />
    // </View>
    <View style={styles.page}>
      <View style={styles.text}>
        <Text style={styles.judul}>Akun</Text>
      </View>
      <View style={styles.content}>
        <Image style={styles.image} source={Park} />
        <Text style={styles.text}>
          Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <Button title="Register" color="green" />
      </View>
    </View>
  );
};

export default Akun;
const windowWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  image: {
    width: windowWidth * 0.9,
    // margin: windowWidth*0.2,
    height: '25%',
  },
  page: {
    flex: 1,
    backgroundColor: 'white',
  },
  judul: {
    fontSize: 25,
    fontFamily: 'Helvetica-Bold',
    color: '#000000',
    marginBottom: 10,
    marginLeft: 10,
  },
  text: {
    color: 'black',
    fontFamily: 'Helvetica',
    fontSize: 18,
    textAlign: 'center',
  },
  content:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
