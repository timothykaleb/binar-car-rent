import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Home, Splash, List, Akun} from '../pages';
import Icon from'react-native-vector-icons/FontAwesome';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused
            ? 'home'
            : 'home';
        } else if (route.name === 'Akun') {
          iconName = focused ? 'user' : 'user';
        }
        else if (route.name === 'List') {
          iconName = focused ? 'list' : 'list';
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: '#0D28A6',
      tabBarInactiveTintColor: 'gray',
    })}
    >
        <Tab.Screen name="Home" component={Home} options={{ headerShown:false}} />
        <Tab.Screen name="List" component={List} options={{ headerShown:false}} />
        <Tab.Screen name="Akun" component={Akun} options={{ headerShown:false}} />
  </Tab.Navigator>
  )
}

const Router = () => {
  return (
     <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} options={{ headerShown:false}} />
        <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown:false}} />
      </Stack.Navigator>
  )
}

export default Router

const styles = StyleSheet.create({})