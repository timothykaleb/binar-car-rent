import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Mobil} from '../../assets';
import Icons from 'react-native-vector-icons/FontAwesome';
// import { ArrowUpCircle } from "react-native-vector-icons/Feather";

const ListMobil = () => {
  const Icon = () => {
    return <Image source={Mobil} style={styles.ImageIcon} />;
  };
  return (
    <TouchableOpacity style={styles.container}>
      <Icon />
      <View style={styles.data}>
        <Text style={styles.text}>Daihatsu Xenia</Text>
        {/* <Text style={styles.keterangan}>Daihatsu Xenia</Text> */}
        <View style={styles.viewIcon}>
          <Icons name="group" size={20} color="black" style={styles.Icon} />
          <Text style={styles.Icon}>4</Text>
          <Icons name="briefcase" size={20} color="black" style={styles.Icon} />
          {/* <ArrowUpCircle /> */}
          <Text style={styles.Icon}>2</Text>
        </View>
        <Text style={styles.harga}>Rp 230.000</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ListMobil;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    width: windowWidth * 0.9,
    paddingHorizontal: 17,
    paddingVertical: 10,
    flexDirection: 'row',
    borderRadius: 4,
    borderColor: 'black',
    marginBottom: 5,
    elevation: 1,
    alignItems: 'center',
  },
  ImageIcon: {
    width: 80,
    height: 48,
    marginRight: windowWidth * 0.08,
  },
  text: {
    color: 'black',
    fontSize: 20,
  },
  harga: {
    color: 'green',
  },
  viewIcon: {
    flex: 1,
    flexDirection: 'row',
  },
  Icon:{
    marginRight:windowWidth * 0.02,
  }
});
