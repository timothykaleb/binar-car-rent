import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const Banner = () => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require('./../../assets/images/Banner.png')}/>
    </View>
  )
}

export default Banner

const styles = StyleSheet.create({
    container: {
        margin:10,
        borderRadius:10,
        
    },
    image: {
        width:'100%',
        height:'100%',
        
    },
})