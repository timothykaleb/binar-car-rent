import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native';

import {IconBox, IconCamera, IconKey, IconTruck} from '../../assets';
import {Box, Camera, Key, Truck} from '../../assets';

const ButtonIcon = ({title}) => {
  const Icon = () => {
    if (title === 'Sewa Mobil')
      return <Image source={Truck} style={styles.ImageIcon} />;
    if (title === 'Oleh-Oleh')
      return <Image source={Box} style={styles.ImageIcon} />;
    if (title === 'Penginapan')
      return <Image source={Key} style={styles.ImageIcon} />;
    if (title === 'Wisata')
      return <Image source={Camera} style={styles.ImageIcon} />;

    return <IconTruck />;
  };

  return (
    <TouchableOpacity>
      <View style={styles.icon}>
        <Icon />
      </View>
      <View style={styles.TextIcon}>
      <Text style={{    fontFamily: 'Helvetica',fontSize:14, color: '#000000',}}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ButtonIcon;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  icon: {
    backgroundColor: '#DEF1DF',
    height: 70,
    width: 70,
    marginHorizontal: windowWidth * 0.01,
    alignItems:'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  ImageIcon: {
    height: '60%',
    width: '60%',
  },
  TextIcon: {
    alignItems:'center',
  }
});
