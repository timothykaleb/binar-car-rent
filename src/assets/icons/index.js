import IconBox from './fi_box.svg'
import IconCamera from './fi_camera.svg'
import IconKey from './fi_key.svg'
import IconTruck from './fi_truck.svg'

export {IconBox, IconCamera, IconKey, IconTruck};