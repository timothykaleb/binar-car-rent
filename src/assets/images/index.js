import SplashBackground from './splash.png'
import Banner from './Banner.png'
import Box from './fi_box.png'
import Camera from './fi_camera.png'
import Key from './fi_key.png'
import Truck from './fi_truck.png'
import ImageHeader from './header.png'
import Mobil from './mobil.png'
import Park from './Allura-Park-1.png'
import Profil from './profil.png'

export {ImageHeader,SplashBackground, Banner, Box, Camera, Key, Truck, Mobil, Park, Profil}